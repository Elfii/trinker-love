var colors = require('colors');
const people = require('./people');
const { byGenre, byInterest, byMovie, byIncome, byCreaListe } = require('./trinker');
const tr = require('./trinker');

console.log(tr.title());
console.log("Model des données : ");
console.log(people[0]);
console.log(tr.line('LEVEL 1'));
console.log("Nombre d'hommes : ",                                                                   tr.nb(people,byGenre,'Male'));
console.log("Nombre de femmes : ",                                                                  tr.nb(people, byGenre,'Female'));
console.log("Nombre de personnes qui cherchent un homme :",                                         tr.nb(people, byInterest,'M'));
console.log("Nombre de personnes qui cherchent une femme :",                                        tr.nb(people, byInterest,'F'));
console.log("Nombre de personnes qui gagnent plus de 2000$ :",                                      tr.nb(people, byIncome, 2000));
console.log("Nombre de personnes qui aiment les Drama :",                                           tr.nb(people, byMovie, 'Drama'));
console.log("Nombre de femmes qui aiment la science-fiction :",                                     tr.nb(tr.byGenre(people,'Female'), byMovie, 'Sci-Fi'));
console.log(tr.line('LEVEL 2'));
console.log("Nombre de personnes qui aiment les documentaires et gagnent plus de 1482$ :",          tr.nb(tr.byIncome(people, '1482'), byMovie, 'Documentary'));
console.log("Liste des noms, prénoms, id et revenu des personnes qui gagnent plus de 4000$ :",      tr.byCreaListe(tr.byIncome(people, 4000)));
console.log("Homme le plus riche (nom et id) :",                                                    tr.byLePlus(tr.byGenre(people,'Male')));
console.log("Salaire moyen :",                                                                      tr.moyenneSalaire(people));
console.log("Salaire médian :",                                                                     tr.salaireMedian(people));
console.log("Nombre de personnes qui habitent dans l'hémisphère nord :",                             tr.personnesHemisphereNord(people));
console.log("Salaire moyen des personnes qui habitent dans l'hémisphère sud :",                      tr.salaireHemisphereSud(people));
console.log(tr.line('LEVEL 3'));            
console.log("Personne qui habite le plus près de Bérénice Cawt (nom et id) :",                      tr.personnePlusProche(people,'Cawt','Bérénice')[1]);
console.log("Personne qui habite le plus près de Ruì Brach (nom et id) :",                          tr.personnePlusProche(people,'Brach','Ruì')[1]);
console.log("les 10 personnes qui habite les plus près de Josée Boshard (nom et id) :",             tr.slice(tr.personnePlusProche(people,'Boshard','Josée')));
console.log("Les noms et ids des 23 personnes qui travaillent chez google :",                       tr.byIdNom(tr.byEmail(people,'google')));
console.log("Personne la plus agée :",                                                              tr.oldDateOfBirth(people));
console.log("Personne la plus jeune :",                                                             tr.youngDateOfBirth(people));
console.log("Moyenne des différences d'age :",                                                      tr.age(people));
console.log(tr.line('LEVEL 4'));            
console.log("Genre de film le plus populaire :",                                                    "create function".blue);
console.log("Genres de film par ordre de popularité :",                                             "create function".blue);
console.log("Liste des genres de film et nombre de personnes qui les préfèrent :",                  "create function".blue);
console.log("Age moyen des hommes qui aiment les films noirs :",                                    "create function".blue);
console.log(`Age moyen des femmes qui aiment les films noirs, habitent sur le fuseau horaire 
de Paris et gagnent moins que la moyenne des hommes :`,                                                 "create function".blue);
console.log(`Homme qui cherche un homme et habite le plus proche d'un homme qui a au moins une 
préférence de film en commun (afficher les deux et la distance entre les deux):`,                   "create function".blue);
console.log("Liste des couples femmes / hommes qui ont les même préférences de films :",            "create function".blue);
console.log(tr.line('MATCH'));
/* 
    On match les gens avec ce qu'ils cherchent (homme ou femme).
    On prend en priorité ceux qui sont les plus proches.
    Puis ceux qui ont le plus de goût en commun.
    Pour les couples hétéroséxuel on s'assure que la femme gagne au moins 10% de moins que l'homme mais plus de la moitié. 
    (C'est comme ça que fonctionne les sites de rencontre ! https://editionsgouttedor.com/catalogue/lamour-sous-algorithme/)
    Les gens qui travaillent chez google ne peuvent qu'être en couple entre eux.
    Quelqu'un qui n'aime pas les Drama ne peux pas être en couple avec quelqu'un qui les aime.
    Quelqu'un qui aime les films d'aventure doit forcement être en couple avec quelqu'un qui aime aussi les films d'aventure.
    Le différences d'age dans un couple doit être inférieure à 25% (de l'age du plus agée des deux)
    ߷    ߷    ߷    Créer le plus de couples possibles.   ߷    ߷    ߷    
    ߷    ߷    ߷    Mesurez le temps de calcul de votre fonction   ߷    ߷    ߷    
    ߷    ߷    ߷    Essayez de réduire le temps de calcul au maximum   ߷    ߷    ߷    
*/
console.log("liste de couples à matcher (nom et id pour chaque membre du couple) :",             tr.match(people));