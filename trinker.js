const { includes, filter, push } = require("./people");
const people = require("./people");

module.exports = {
    title: function (){
        return `
$$$$$$$$\\        $$\\           $$\\                           $$\\                               
\\__$$  __|       \\__|          $$ |                          $$ |                              
   $$ | $$$$$$\\  $$\\ $$$$$$$\\  $$ |  $$\\  $$$$$$\\   $$$$$$\\  $$ | $$$$$$\\ $$\\    $$\\  $$$$$$\\  
   $$ |$$  __$$\\ $$ |$$  __$$\\ $$ | $$  |$$  __$$\\ $$  __$$\\ $$ |$$  __$$\\\\$$\\  $$  |$$  __$$\\ 
   $$ |$$ |  \\__|$$ |$$ |  $$ |$$$$$$  / $$$$$$$$ |$$ |  \\__|$$ |$$ /  $$ |\\$$\\$$  / $$$$$$$$ |
   $$ |$$ |      $$ |$$ |  $$ |$$  _$$<  $$   ____|$$ |      $$ |$$ |  $$ | \\$$$  /  $$   ____|
   $$ |$$ |      $$ |$$ |  $$ |$$ | \\$$\\ \\$$$$$$$\\ $$ |$$\\   $$ |\\$$$$$$  |  \\$  /   \\$$$$$$$\\ 
   \\__|\\__|      \\__|\\__|  \\__|\\__|  \\__| \\_______|\\__|\\__|  \\__| \\______/    \\_/     \\_______|
================================================================================================
   `.yellow
    },


    // exo corrigé = créer des fonctions généralisées, fonction de filtre "byGenre"  "byInterest" "byMovie"... Ensuite fonction de contrôl "femmeSF" = "byGenre" + "byMovie" 
    //fonction de view nb: function(People,filtre,paramètre) {let Filtre = filtre(People,Paramètre); return filtre.length;}, 
    //console.log(nb(P,byGenre, "female"));





    line: function(title = "="){
        return title.padStart(48, "=").padEnd(96, "=").bgBrightYellow.black
    },

    // allMale: function(p){
    //     return p.filter(i=>i.gender == 'Male');
    // },

    // allFemale: function(p){
    //     return p.filter(i=>i.gender == 'Female');
    // },

    // nbOfMale: function(p){
    //     return this.allMale(p).length;
    // },

       // nbOfFemale: function(p){
    //     return this.byGenre(p).length;
    // },


    // likeMale: function(p){
    //     return p.filter(i=>i.looking_for == 'M');
    // },

    // nbOfMaleInterest: function(p){
    //     return this.likeMale(p).length;
    // },

    // likeFemale: function(p){
    //     return p.filter(i=>i.looking_for == 'F');
    // },

    // nbOfFemaleInterest: function(p){
    //     return this.likeFemale(p).length;
    // },

    // miniIncome2000: function(p){
    //     return p.filter(i=>parseFloat((i.income).slice(1)) >='2000');
    // },

    // nbrMiniIncome2000: function(p){
    //     return this.miniIncome2000(p).length;
    // },

    // personneLikeDrama: function(p){
    //     return p.filter(i=>i.pref_movie.includes('Drama'));
    // },

    // nbrPersonneLikeDrama: function(p){
    //     return this.personneLikeDrama(p).length;
    // },

    // Science_fiction: function(p){
    //     return p.filter(i=>i.pref_movie.includes('Sci-Fi'));
    // },

    // nbrFemmeLikeScience_fiction: function(p){
    //     return this.Science_fiction(this.byGenre(p,'Female')).length;
    // },

    // documentaires: function(p){
    //     return p.filter(i=>i.pref_movie.includes('Documentary'));
    // },

    // gagnentPlusDe1482$: function(p){
    //     return p.filter(i=>parseFloat((i.income).slice(1)) >='1482');
    // },

    // nbrPersonnesLikeDocEtGagnentPlusDe$: function(p){
    //     return this.documentaires(this.gagnentPlusDe1482$(p)).length;
    // },

    // gagnentPlusDe4000$: function(p){
    //     return p.filter(i=>parseFloat((i.income).slice(1)) >='4000');
    // }  

    byGenre: function(p,g){                 // Tableau de personne "P" par la donnée "male" ou "female"
        return p.filter(i=>i.gender === g); // en string "G" ; Return tableau de personne filtré par genre;
    },

    byInterest: function(p,int){                    // Tableau de personne "P" par la donnée  intéressé par "m" ou "f"
        return p.filter(i=>i.looking_for === int);  // en string "int" ; Return tableau de personne filtré par intéret;
    },

    byIncome: function(p,inc){                                     // Tableau de personne "P" par la donnée salaire "inc", emputé du $ pour pouvoir le convertir
        return p.filter(i=>parseFloat((i.income).slice(1)) >= inc); // en nombre flotant ; Return tableau de personne filtré par leur salaire;
    },

    byMovie: function(p,movie){                          //Tableau de personne "P" par la donnée film "movi" en string;
        return p.filter(i=>i.pref_movie.includes(movie));// Return Tableau de personne filtré par un style de film;
    },

    nb: function(people, filtre,param){       //Tableau de personne "People" avec un filtre "filtre" et un paramètre "param" en string défini dans algo.js;
        let filtré = filtre(people,param);    // Return un Nombre "integer" de personne filtré ; 
        return filtré.length;
    },



    byCreaListe: function(p){               //Tableau de personne "P", par id, nom, prénom et salaire en string;
        return p.map(i=>{                   // Return un tableau en forme de liste par les éléments demandés;                
            
                let id = i.id;
                let name = i.first_name;
                let lastName = i.last_name;
                let income = i.income;
                
            
        
        return [id, name, lastName, income];
    })
    },

    byLePlus: function(p){         // Tableau de personne "P" par salaire (string), id (integer) et nom (string);
        let nombres = []           // Return la dernière personne d'un tableau trié par ordre croissant sous forme de tableau, avec Id et nom;
        for(let elem of p){
            salaire = parseFloat((elem.income).slice(1))
            nombres.push([salaire, elem.id, elem.last_name])
        }
        let triee = nombres.sort( // tri du plus petit au plus grand
            function(a, b) {
            return a[0] - b[0]; // a.InCome - b.InCome
        });
        return triee[triee.length-1]    // lenght-1 = le dernier element du tableau qui est donc le plus grand
    },
    

    
    moyenneSalaire: function(p) {   //Tableau de personne "P" par le salaire en string;
    let moyenne = 0;                // Return la moyenne des salaires en nombre entier flotant;
    for (let elem of p ){
        moyenne += parseFloat((elem.income).slice(1));
    }
    return moyenne = moyenne/p.length;
    
},
    salaireMedian: function(p) {      //Tableau de personne "P" par le salaire en Integer; Return                        
        let salaire = p.map(i=> parseFloat(i.income.slice(1))).sort((a,b)=>a-b);
        let median = Math.floor(salaire.length/2);
        return (salaire[median -1] + salaire[median]) /2; 
},

    personnesHemisphereNord: function(p){                  // Tableau de personne "P" filtré par "lalitude > 0" en Number;
        let hemisphereNord = p.filter(i=> i.latitude > 0); // Return le nombre de Personne en intenger habitant dans l'hemisphère Nord;
        return hemisphereNord.length;
    },

    salaireHemisphereSud: function(p){                   // Tableau de personne "P" filtré par "lalitude < 0" en Number;
        let moyenne = 0;                                 // Return la moyenne des salaires des personnes vivant dans l'hemisphère sud en Intenger;
        let hemisphereSud = p.filter(i=>i.latitude < 0);
    for (let elem of hemisphereSud ){
        moyenne += parseFloat((elem.income).slice(1));
    }
    return hemisphereSud = moyenne/hemisphereSud.length;
    },
        
    byEmail: function(p, em){                      // Tableau de personne "P" filtré par mail de type string;
        return p.filter(i=>i.email.includes(em)); // Return un tableau de personne filtré par adresse mail;
    },

    byIdNom: function(p){            // Tableau de personne "p" par Id(integer) et nom (string);
        return p.map(i=>{            // Return un tableau avec les données ID en intenger et name en string;

            let id= i.id;
            let name= i.first_name;
            return[id, name];
        })
    },

    oldDateOfBirth: function(p){     // Tableau de personne "p" par age (string);
        p.sort((a,b)=>               // Return le premier index d'un tableau "p" trié sous forme d'objet;
                new Date(a.date_of_birth) - new Date(b.date_of_birth));
            return p[0];
        },

    youngDateOfBirth: function(p){       // Tableau de personne "p" par age (string)
        p.sort((a,b)=>                   // Return le premier index d'un tableau "p" trié sous forme d'objet;
                new Date(b.date_of_birth) - new Date(a.date_of_birth));
                return p[0];
        },
    
    
    people: function(p){                 // Tableau de personne "p" par longitude et latitude (nombre entier flotant);
        return p.map(i=>{                // Return un tableau avec ses données en nombre entier flotant;
            let long = i.longitude;
            let lat = i.latitude;
            
        return [long,lat]; 
        })
},
    
distanceEntree: function(p1,p2){                         // Donnée de valeur "p.1" et "p.2" par longitude et latitude (nombre entier flotant);
    let a = p1.latitude - p2.latitude;                   // return le resultat d'une formule en (nombre entier flotant);
    let b = p1.longitude - p2.longitude;
    let d = Math.sqrt(Math.pow(a, 2) + Math.pow(b, 2))
    return d;
},

personnePlusProche: function(people, nom, prenom){                                       // Tableau de personne "people" filtré par "nom" et "prenom" en string;
        trie1 = people.filter(personne => personne.last_name === nom)                    // Return la personne la plus proche sous forme d'objet avec son id (integer) et son nom(string),
        lapersonne = trie1.filter(personne => personne.first_name === prenom)[0]         // d'une personne donné;
        coordonée = {latitude: lapersonne.latitude, longitude: lapersonne.longitude}
        distanceentre = []
        for(let personne of people){
            variable = {latitude: personne.latitude, longitude: personne.longitude}
            distance = this.distanceEntree(coordonée, variable)
            distanceentre.push([distance, personne.id, personne.last_name])
        }
        distance = distanceentre.sort(function(a,b){return a[0]-b[0]});
        return distance
        },

slice: function(p){         // Tableau de personne "p";
    return p.slice(1,11);   // return un tableau de 10 peronnes;
},

age: function(p){                            // function non fini pour la moyenne d'âge xD
            let a = {a: a.date_of_birth}; 
            let b = {b: b.date_of_birth};
            for(let all ; all>=p; all ++){
                return all = a-b;
            }
            },



    // on a des gens : 1--> pref_movie : A|B|E|Zombie
    //                 2 --> pref_movie : A|C|
    // créér un objet pour les scores : let score = {};
    //                                 for each(p)
    // quel personne aime quels films? let films = (regroupé dans une chaine de caractère donc doit être découpé)
    //                                 let film = p.pref_movie.split("|") (veut dire à chaque fais moi un tableau avec des éléménts de chaque | )    
    //                                   for(let F of films){}
    //                                      score[F] +=1;
    //                                      if(!score[F]){
    //                                          score[F]=0
    //                                      } 
    //                                      score[F] +=1;
                                    //    }
                                    //  }
    
    match: function(p){
        return "not implemented".red;
    }
    //     return Math.sqrt(Math.pow((p = i=>i.latitude - bob = i=>i.latitude)/2) + Math.pow((p.longitude - bob.longitude)/2)); 
    
}
